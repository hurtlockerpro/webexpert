<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;
use Twig\Environment;

class Mailer
{
    private $mailer;
    private $twig;
    private $entrypointLookup;

    public function __construct(MailerInterface $mailer, Environment $twig, EntrypointLookupInterface $entrypointLookup)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->entrypointLookup = $entrypointLookup;
    }

    public function sendWelcomeMessage(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('alienmailcarrier@example.com', 'Vladimir Kjahrenov'))
            ->to('info@hurtlocker.pro')
            ->subject('Welcome!')
            ->htmlTemplate('emails/welcome.html.twig')
            ->context([
                // You can pass whatever data you want
                //'user' => $user,
            ]);

        $this->mailer->send($email);
    }

    public function sendAuthorWeeklyReportMessage(array $news)
    {
        $this->entrypointLookup->reset();
        /*
        $html = $this->twig->render('emails/author-weekly-report-pdf.html.twig', [
            'news' => $news,
        ]);
        */

        $email = (new TemplatedEmail())
            ->from(new Address('info@hurtlocker.pro', 'Vladimir Kjahrenov'))
            ->to('info@hurtlocker.pro')
            ->subject('Your top 10 weekly news!')
            ->htmlTemplate('emails/weekly-top-news.html.twig')
            ->context([
                'news' => $news,
            ]);

        $this->mailer->send($email);
    }
}
