-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Loomise aeg: Veebr 10, 2020 kell 12:14 PL
-- Serveri versioon: 5.7.19
-- PHP versioon: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Andmebaas: `webexpert`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `category`
--

INSERT INTO `category` (`id`, `title`) VALUES
(1, 'Esimene'),
(2, 'Teine'),
(3, 'Kolmas');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `comment` varchar(160) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `comments`
--

INSERT INTO `comments` (`id`, `news_id`, `comment`) VALUES
(1, 5, 'this is comment 1'),
(2, 5, 'this is comment 2');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Andmete tõmmistamine tabelile `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200207164407', '2020-02-07 16:44:39');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description_short` varchar(500) NOT NULL,
  `description_long` varchar(1000) NOT NULL,
  `datetime` datetime NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1DD3995012469DE2` (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `news`
--

INSERT INTO `news` (`id`, `category_id`, `title`, `description_short`, `description_long`, `datetime`, `image`) VALUES
(1, 1, 'this is news title', 'dvfwef  description short', 'description long', '2020-01-16 00:00:00', NULL),
(2, 1, 'this is news 2', 'short description 2', 'long description 2', '2020-02-07 00:00:00', NULL),
(3, 2, 'news title 3', 'short description 3', 'long description 3', '2020-02-06 03:17:00', NULL),
(4, 1, 'news title 4', 'short description 4', 'long description 4', '2020-01-16 00:00:00', NULL),
(5, 2, 'news title 5', 'short description 5', 'long description 5', '2020-02-20 00:00:00', NULL),
(6, 1, 'news title 6', 'short description 6', 'long description 6', '2020-02-18 00:00:00', NULL),
(7, 2, 'news title 7', 'short description 7', 'long description 7', '2020-02-16 00:00:00', NULL),
(8, 3, 'news 333', 'short description', 'this is good news', '2015-01-01 00:00:00', 'C:\\serverPHP\\tmp\\php6AE2.tmp');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
