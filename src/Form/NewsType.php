<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category_id', EntityType::class,[
                'class' => 'App:Category',
                'choice_label' => 'title',
            ])
            ->add('title')
            ->add('description_short')
            ->add('description_long', TextareaType::class, ['attr'=> array('rows' => 5 , 'cols' => 70)])
            ->add('datetime')
            /*
            ->add('imageFile', FileType::class, [
                'mapped' => false,
                //'required' => false,
            ])*/;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
