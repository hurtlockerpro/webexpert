<?php

namespace App\Command;


use App\Repository\NewsRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class WeeklyReportSendCommand extends Command
{
    protected static $defaultName = 'app:weekly-report:send';

    private $newsRepository;
    private $mailer;

    public function __construct(NewsRepository $newsRepository, MailerInterface $mailer)
    {
        parent::__construct(null);
        $this->newsRepository = $newsRepository;
        $this->mailer = $mailer;
    }

    protected function configure()
    {
        $this
            ->setDescription('This is weekly TOP 10 news sender');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $news = $this->newsRepository->findTop10News();

        print_r ("Sending emails");

        $email = (new TemplatedEmail())
            ->from(new Address('info@hurtlocker.pro', 'Vladimir Kjahrenov'))
            ->to("info@hurtlocker.pro") // TODO: change email here to upropriate one
            ->subject('Your top 10 weekly news!')
            ->htmlTemplate('emails/weekly-top-news.html.twig')
            ->context([
                'top10news' => $news,
            ]);
        $this->mailer->send($email);

        //$io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
