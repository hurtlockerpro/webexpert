<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\News;
use App\Form\NewsType;
use App\Repository\CommentsRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Gedmo\Sluggable\Util\Urlizer;

/**
 * @Route("/news")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/{page<\d+>?1}", name="news_index", methods={"GET"})
     */
    public function index($page = 1, NewsRepository $newsRepository): Response
    {

        if (empty($page)) $page = 1;

        // ... get posts from DB...
        // Controller Action
        $newsAll = $newsRepository->getAllNews($page); // Returns 5 posts out of 20

        // You can also call the count methods (check PHPDoc for `paginate()`)
        # Total fetched (ie: `5` posts)
        $totalPostsReturned = $newsAll->getIterator()->count();

        # Count of ALL posts (ie: `20` posts)
        $totalPosts = $newsAll->count();

        # ArrayIterator
        $iterator = $newsAll->getIterator();

        $limit = 5;
        $maxPages = ceil($totalPosts / $limit);


        return $this->render('news/index.html.twig', [
            'news' => $newsAll, //$newsRepository->findAll(),
            'categories',
            'maxPages' => $maxPages,
            'thisPage'=> $page
        ]);
    }

    /**
     * @Route("/new", name="news_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="news_show", methods={"GET","POST"})
     */
    public function show(News $news, Request $request): Response
    {
        //$comments = $commentRepository->findBy(['news' => $news]);
        $comments = $news->getComments();

        $comment = new Comments();
        $form = $this->createFormBuilder($comment)
            ->add('comment',     TextareaType::class, [
                //'data' => 'hello',
                'attr' => ['rows' => 3, 'cols' => 35],
            ])
            ->getForm();


        //$request = $this->get('request');

        if ($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $news->addComments($comment);
                $em = $this->getDoctrine()->getManager();
                $em->persist($news);
                $em->persist($comment);
                $em->flush();
            }
        }



        return $this->render('news/show.html.twig', [
            'news' => $news,
            'comments' => $comments,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="news_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, News $news): Response
    {
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_delete", methods={"DELETE"})
     */
    public function delete(Request $request, News $news): Response
    {
        if ($this->isCsrfTokenValid('delete'.$news->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($news);
            $entityManager->flush();
        }

        return $this->redirectToRoute('news_index');
    }

    /**
     * @Route("/newsByCategory/{categoryId<\d+>}/{page<\d+>?1}", name="news_by_category")
     */
    public function newsByCategory($categoryId, $page = 1, NewsRepository $newsRepository) : Response
    {
        if (empty($page)) $page = 1;
        if (empty($categoryId)) $categoryId  = 1;

        // ... get posts from DB...
        // Controller Action
        $newsAll = $newsRepository->getAllNews($page, $categoryId); // Returns 5 posts out of 20

        // You can also call the count methods (check PHPDoc for `paginate()`)
        # Total fetched (ie: `5` posts)
        $totalPostsReturned = $newsAll->getIterator()->count();

        # Count of ALL posts
        $totalPosts = $newsAll->count();

        # ArrayIterator
        $iterator = $newsAll->getIterator();

        $limit = 5;
        $maxPages = ceil($totalPosts / $limit);


        return $this->render('news/index.html.twig', [
            'news' => $newsAll,
            'categories',
            'maxPages' => $maxPages,
            'thisPage'=> $page
        ]);

    }
}
