<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;

class bLoadNewsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // create 10 news
        for ($i = 1; $i <= 10; $i++) {

            for($j = 1; $j <= 5; $j++) {
                $n = new News();
                $n->setCategory($this->getReference('category.abstract.' . $j));
                $n->setTitle('news nr. ' . $i . '_' . $j);
                $n->setDescriptionShort('short description nr. ' . $i . '_' . $j);
                $n->setDescriptionLong('long description nr. ' . $i . '_' . $j);
                $n->setDatetime(new \DateTime());

                $path = __DIR__.'/../../public/uploads/NO_IMG_600x600.png';
                $imageFile = new File($path);
                $n->setImageFile($imageFile);

                $n->setImage('NO_IMG_600x600.png');

                $this->addReference('news.abstract.' . $i . '_' . $j, $n);

                $manager->persist($n);
            }
        }
        $manager->flush();

    }
}
