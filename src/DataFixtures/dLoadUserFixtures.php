<?php

namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class dLoadUserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // add admin user
        $u = new User();
        $u->setUsername('admin');
        $u->setRoles(["ROLE_ADMIN"]);
        $u->setPassword('$argon2id$v=19$m=65536,t=4,p=1$ckFNN2RMODc2VVljcUtzOA$ao+HjpkaOCrF8z4Poan8EYEbBmQSbn8AxC5ura7obO8');

        $manager->persist($u);

        $manager->flush();
    }
}
