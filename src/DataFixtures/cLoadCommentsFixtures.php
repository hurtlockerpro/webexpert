<?php

namespace App\DataFixtures;

use App\Entity\Comments;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class cLoadCommentsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // add some comments
        for ($i = 1; $i <= 10; $i++)
        {
            for($j = 1; $j <= 3; $j++)
            {
                $c = new Comments();
                $c->setNews($this->getReference('news.abstract.' . $i . '_' . $j));
                $c->setComment('kommentaar (uudis: ' . $i . ') nr. ' . $j);

                $manager->persist($c);
            }
        }
        $manager->flush();
    }
}
