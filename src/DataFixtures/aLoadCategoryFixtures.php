<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class aLoadCategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // add 5 categories
        for ($i = 1; $i <= 5; $i++) {
            $cat = new Category();
            $cat->setTitle('My Category ' . $i);

            $this->addReference('category.abstract.' . $i, $cat);

            $manager->persist($cat);
        }
        $manager->flush();

    }
}
