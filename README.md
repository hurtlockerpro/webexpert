# Test for apply for a job in WebExpert 

# TEST USER WITH ADMIN ROLE 
* username : admin 
* password: admin

This project was done using Symfony framework (https://symfony.com/) version 4.4 <br><br>
Additionally used modules: 
 * easyadmin
 * bootstrap 4
 * PHP 7.1.9
	
## How to install/build app

* Clone this project from: `git clone https://gitlab.com/hurtlockerpro/webexpert.git`
* Run `composer install`

## Database - migration and fixtures 
Change database connection username and password like this
* Go to .env file and find DATABASE_URL and set right username and password. Database name will be 'webexpert'

To create databse run following commands:
* Create database: `php bin/console doctrine:database:create`
* Create required tables `php bin/console doctrine:schema:update --force`
* Install demo content, run fixtures `php bin/console doctrine:fixtures:load`

## Email server for sending emails
 
 NB! I used smtp.mailtrap.io If you have your own account on that servise then do next steps:
 * Go to .env file and find MAILER_DSN 
 * then change username:password  -> (between smtp:// and @smtp.mailtrap.io:2525). NB! username and password is separated with colon ':'
 
## Run symfony server
* Run `symfony serve`
* Go to `https://127.0.0.1:8000/` NB!!! Port number may differ ! if everything is ok then you will see news list 

## Sending email with last week "TOP 10 news", using inner symfony service 
If smtp server is set successfully the you can send tOP 10 news from command line 
* Go to command/WeeklyReportSendCommand and change row 47 > change email where you want to send emails 
* Run command: `php bin/console app:weekly-report:send`


## Contact

Test exersize for WebExpert is done by:

Name: Vladimir Kjahrenov <br>
Tel: +372 55 621 935<br>
E-mail: info@hurtlocker.pro<br>
Skype: hurtlockerpro
